# Basket
`Тестовое задание для `**`"Лаборатория ШИФТ"`**
## Аргументы
Приложение не имеет никаких данных в памяти о каких-либо товарах на старте. Потому требуется указать файл, в котором написаны товары в таком формате:
```
<Название товара>, <Цена товара>
<Название товара>, <Цена товара>
...
```
Например, вариант для тестового примера из условий задания:
```
Агава, 100.5
Бумага, 11.1
```
Для этого используется аргумент **`--goods <путь_к_файлу>.`**
## Товары
Товары, которые клиент хочет пометстить в корзину читаются приложением из стандартного потока ввода в следующем формате(аналогичный требованию задания):
```
<Название товара> <Количество товара>
<Название товара> <Количество товара>
...
```