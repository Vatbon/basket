package entities;

import service.util.IdFactory;

public class User {
    private final int uniqueId;
    private final String fullName;

    public User(String fullName) {
        this.fullName = fullName;
        uniqueId = IdFactory.produceUserId();
    }

    private User(String fullName, int uniqueId) {
        this.fullName = fullName;
        this.uniqueId = uniqueId;
    }

    /**
     * @return Стандартного пользователя с fullName = "Фамилия Имя Отчество" и uniqueId = -1;
     */
    public static User getDefaultUser() {
        return new User("Фамилия Имя Отчество", -1);
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public String getFullName() {
        return fullName;
    }
}
