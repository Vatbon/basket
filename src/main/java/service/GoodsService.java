package service;

import entities.Good;
import service.util.entities.UtilGoodEntity;
import exception.GoodAlreadyExistException;
import repository.GoodsRepository;
import repository.implementation.InMemoryGoodsRepository;
import service.util.Formatter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class GoodsService {

    private final GoodsRepository repository = new InMemoryGoodsRepository();


    /**
     * Данный метод создает чек на основе переданной строки.
     * Не проверяет корректность введеной строки.
     *
     * @param goods Строки разделенные символом новой строки товаров и пробелом между названием и количеством.
     * @return Строку, содержащую "чек".
     */
    public String makeBasketFromString(String goods) {
        List<UtilGoodEntity> goodList = new ArrayList<>();
        Formatter formatter = new Formatter();
        /*
         * Разделяем строку на подстроки по символу новой строки.
         * Потом на компоненты "имя" и "кол-во" по пробелу.
         */
        goods.lines()
                .parallel()
                .flatMap(l -> Stream.of(l.split("\\n")))
                .filter(l -> l.length() > 0)
                .forEach(line -> {
                    String[] tempString = line.trim().split("\\s");
                    if (tempString.length == 2)
                        goodList.add(new UtilGoodEntity(tempString[0], Integer.parseInt(tempString[1])));
                });

        int maxNameLen = 1;
        int maxPriceLen = 1;
        int maxQuantityLen = 1;
        boolean hasExistingGoods = false;
        boolean hasNotExistingGoods = false;
        /*
         * Для каждого товара проверяем, есть ли он в репозитории.
         * Если есть, то делаем пометку hasExistingGoods = true
         */
        for (UtilGoodEntity utilGoodEntity : goodList) {
            if (repository.containsEntityByName(utilGoodEntity.name)) {
                utilGoodEntity.price = repository.getEntityByName(utilGoodEntity.name).getPrice();
                utilGoodEntity.exists = true;
                maxNameLen = Math.max(maxNameLen, utilGoodEntity.name.length());
                maxPriceLen = Math.max(maxPriceLen, String.format("%.1f", utilGoodEntity.price).length());
                maxQuantityLen = Math.max(maxQuantityLen, String.format("%d", utilGoodEntity.quantity).length());
                hasExistingGoods = true;
            } else {
                utilGoodEntity.exists = false;
                hasNotExistingGoods = true;
            }
        }

        formatter.setMaxNameLen(maxNameLen);
        formatter.setMaxPriceLen(maxPriceLen);
        formatter.setMaxQuantityLen(maxQuantityLen);
        formatter.setHasExistingGoods(hasExistingGoods);
        formatter.setHasNotExistingGoods(hasNotExistingGoods);

        return formatter.buildBasketString(goodList);
    }

    /**
     * @param goodsString Строки разделенные символом новой строки товаров.
     * @param replace     Если true, то товоры с совпадающем названием будут заменены новыми.
     */
    public void loadGoodsIntoRepository(String goodsString, boolean replace) {
        goodsString.lines()
                .parallel()
                .flatMap(l -> Stream.of(l.split("\\n")))
                .filter(l -> l.length() > 0)
                .forEach(line -> {
                    String[] tempString = line.trim().split(",");
                    if (tempString.length == 2 && Double.parseDouble(tempString[1]) > 0) {
                        try {
                            repository.putEntity(new Good(tempString[0], Double.parseDouble(tempString[1])), replace);
                        } catch (GoodAlreadyExistException ignored) {
                        }
                    }
                });
    }
}
