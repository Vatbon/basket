package service.util;

/**
 * Фабрика уникальный идентификаторов.
 */
public class IdFactory {

    private static int amountOfOrders = 0;
    private static int amountOfUsers = 0;
    private static final Object mutexOrder = new Object();
    private static final Object mutexUsers = new Object();

    private IdFactory() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * @return Уникальный номер корзины
     */
    public static int produceBasketNumber() {
        synchronized (mutexOrder) {
            amountOfOrders++;
            return amountOfOrders;
        }
    }

    /**
     * @return Уникальный номер пользователя
     */
    public static int produceUserId() {
        synchronized (mutexUsers) {
            amountOfUsers++;
            return amountOfUsers;
        }
    }
}
