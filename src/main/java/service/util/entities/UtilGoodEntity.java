package service.util.entities;

public class UtilGoodEntity {
    public final String name;
    public double price;
    public final int quantity;
    public boolean exists = false;

    public UtilGoodEntity(String name, int quantity) {
        this.name = name;
        price = -1;
        this.quantity = quantity;
    }
}
