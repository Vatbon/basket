package service.util;

import entities.User;
import service.util.entities.UtilGoodEntity;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Построитель "чека".
 */
public class Formatter {

    private static final String EMPTYBASKETMESSAGE = "Невозможно создать корзину, которая не содержит товаров.";

    private int maxNameLen = "Название".length();
    private int maxPriceLen = "Цена".length();
    private int maxQuantityLen = "Количество".length();
    private boolean hasExistingGoods = false;
    private boolean hasNotExistingGoods = false;
    private User user = null;

    /**
     * @param maxNameLen Максимальная длина имени товара
     */
    public void setMaxNameLen(int maxNameLen) {
        this.maxNameLen = Math.max(maxNameLen, "Название".length());
    }

    /**
     * @param maxPriceLen Максимальная длина цены товара
     */
    public void setMaxPriceLen(int maxPriceLen) {
        this.maxPriceLen = Math.max(maxPriceLen, "Цена".length());
    }

    /**
     * @param maxQuantityLen Максимальная длина числа количества товара
     */
    public void setMaxQuantityLen(int maxQuantityLen) {
        this.maxQuantityLen = Math.max(maxQuantityLen, "Количество".length());
    }

    public void setHasExistingGoods(boolean param) {
        this.hasExistingGoods = param;
    }

    public void setHasNotExistingGoods(boolean param) {
        this.hasNotExistingGoods = param;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String buildBasketString(List<UtilGoodEntity> goodList) {
        StringBuilder stringBuilder = new StringBuilder();

        if (!hasExistingGoods) {
            if (hasNotExistingGoods)
                return EMPTYBASKETMESSAGE + "\n" + stringBuilder.append(makeMissingGoodsString(goodList));
            return EMPTYBASKETMESSAGE;
        }

        stringBuilder.append(makeStringBasketHeader());
        stringBuilder.append(makeStringBasketBody(goodList));
        if (hasNotExistingGoods)
            stringBuilder.append(makeMissingGoodsString(goodList));
        return stringBuilder.toString();
    }

    private String makeStringBasketHeader() {
        int orderNumber = IdFactory.produceBasketNumber();
        Date date = new Date();
        if (user == null)
            user = User.getDefaultUser();
        return new StringBuilder().append("Заказ №").append(orderNumber).append(" ").append(user.getFullName()).append(" ").append(new SimpleDateFormat("yyyy.MM.dd").format(date)).append("\n").toString();
    }

    private String makeStringBasketBody(List<UtilGoodEntity> goodList) {
        StringBuilder stringBuilderResult = new StringBuilder();

        double totalPrice = 0;
        stringBuilderResult.append("\n");

        String stringHeaderFormat = new StringBuilder().append("%-").append(maxNameLen)
                .append("s %-").append(maxPriceLen)
                .append("s %-").append(maxQuantityLen)
                .append("s %s\n").toString();
        String stringGoodFormat = new StringBuilder().append("%-").append(maxNameLen)
                .append("s %-").append(maxPriceLen)
                .append(".1f %-").append(maxQuantityLen)
                .append("d %.1f\n").toString();

        stringBuilderResult.append(String.format(stringHeaderFormat, "Название", "Цена", "Количество", "Сумма"));

        /* Сортируем товар по имени*/
        goodList.sort(Comparator.comparing(o -> o.name));

        for (UtilGoodEntity utilGoodEntity : goodList) {
            if (utilGoodEntity.exists) {
                stringBuilderResult.append(String.format(stringGoodFormat, utilGoodEntity.name, utilGoodEntity.price, utilGoodEntity.quantity, utilGoodEntity.price * utilGoodEntity.quantity));
                totalPrice += utilGoodEntity.price * utilGoodEntity.quantity;
            }
        }

        stringBuilderResult.append("\n").append(String.format("Итого: %.1f%n", totalPrice));
        return stringBuilderResult.toString();
    }

    /**
     * @param goodList Список товаров
     * @return Строку, которая содержит "Не удалось найти следующие товары:" и на каждой следующей строке названием
     * товара, которйы не удалось найти в репозитории
     */
    private String makeMissingGoodsString(List<UtilGoodEntity> goodList) {
        StringBuilder stringBuilderMissing = new StringBuilder();
        stringBuilderMissing.append("\nНе удалось найти следующие товары:\n");
        for (UtilGoodEntity utilGoodEntity : goodList) {
            if (!utilGoodEntity.exists)
                stringBuilderMissing.append(utilGoodEntity.name).append("\n");
        }
        stringBuilderMissing.deleteCharAt(stringBuilderMissing.length() - 1);
        return stringBuilderMissing.toString();
    }
}
