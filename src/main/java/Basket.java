import service.GoodsService;

import java.io.*;

public class Basket {

    private static final String INPUTGOODSFILE_ARGUMENT = "--goods";
    private static final int AMOUNTOFLINESATONCE = 100;

    public static void main(String[] args) {

        GoodsService goodsService = new GoodsService();

        for (int i = 0; i < args.length; i++) {
            /*
             * Если находим аргумент INPUTGOODSFILE_ARGUMENT, то
             * То открываем его для чтения и читаем по AMOUNTOFLINESATONCE строк,
             * После чего добавляем их в репозиторий товаров
             */
            if (args[i].equals(INPUTGOODSFILE_ARGUMENT) && i + 1 < args.length) {
                File file = new File(args[i + 1].trim());

                if (file.isFile() && file.canRead())
                    try (
                            BufferedReader reader = new BufferedReader(new FileReader(file))
                    ) {
                        String buffString;
                        String concatBuffString = "";
                        int j;

                        while (true) {
                            buffString = reader.readLine();
                            if (buffString == null)
                                break;
                            j = 0;

                            while (j < AMOUNTOFLINESATONCE && concatBuffString != null) {
                                concatBuffString = reader.readLine();
                                if (concatBuffString != null)
                                    buffString = buffString.concat("\n").concat(concatBuffString);
                                j++;
                            }

                            goodsService.loadGoodsIntoRepository(buffString, true);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }
        }

        /*
         * Читаем список товаром для корзины из стандартного потока ввода
         */
        StringBuilder stringBuilder = new StringBuilder();
        InputStreamReader isReader = new InputStreamReader(System.in);
        BufferedReader bufReader = new BufferedReader(isReader);
        while (true) {
            try {
                String bufString = bufReader.readLine();
                if (bufString == null)
                    break;
                stringBuilder.append(bufString).append("\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println(goodsService.makeBasketFromString(stringBuilder.toString()));
    }
}

