package repository.implementation;

import entities.Good;
import exception.GoodAlreadyExistException;
import repository.GoodsRepository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryGoodsRepository implements GoodsRepository {

    private final Map<String, Good> goods;

    public InMemoryGoodsRepository() {
        goods = new ConcurrentHashMap<>();
    }

    @Override
    public Good getEntityByName(String goodName) {
        return goods.get(goodName);
    }

    @Override
    public void putEntity(Good good) throws GoodAlreadyExistException {
        String newGoodName = good.getName();
        if (goods.containsKey(newGoodName))
            throw new GoodAlreadyExistException();
        else
            goods.put(newGoodName, good);
    }

    @Override
    public void putEntity(Good good, boolean replace) throws GoodAlreadyExistException {
        if (!replace)
            putEntity(good);
        else if (goods.containsKey(good.getName()))
            goods.replace(good.getName(), good);
        else
            try {
                putEntity(good);
            } catch (GoodAlreadyExistException ignored) {
            }

    }

    @Override
    public boolean containsEntityByName(String name) {
        return goods.containsKey(name);
    }
}
