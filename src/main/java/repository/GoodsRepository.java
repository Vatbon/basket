package repository;

import entities.Good;
import exception.GoodAlreadyExistException;

public interface GoodsRepository {
    /**
     * @param name Имя товара
     * @return Товар, если такое имя есть, иначе null
     * @see java.util.Map#get(Object)
     */
    Good getEntityByName(String name);

    /**
     * @param good Товар
     * @throws GoodAlreadyExistException Если товар с похожим именем существует
     */
    void putEntity(Good good) throws GoodAlreadyExistException;

    /**
     * @param good Товар
     * @param replace Если true, то товары с совпадающем названием будут заменены новыми.
     * @throws GoodAlreadyExistException Если replace = false и товар с данным именем уже существует
     */
    void putEntity(Good good, boolean replace) throws GoodAlreadyExistException;


    /**
     * @param name Имя товара
     * @return true, если товар с таким названием сущетсвует, иначе false
     */
    boolean containsEntityByName(String name);
}
