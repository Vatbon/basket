import org.junit.Assert;
import org.junit.Test;
import service.GoodsService;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RunTest {
    @Test
    public void RunTest() {
        Assert.assertNotNull(new GoodsService());
    }

    @Test
    public void EmptyBasket() {
        GoodsService goodsService = new GoodsService();
        String inputGoodsString = "Агава, 100.5\n Бумага, 11.3";
        String inputBasketString = "";
        goodsService.loadGoodsIntoRepository(inputGoodsString, true);
        String result = goodsService.makeBasketFromString(inputBasketString);
        Assert.assertEquals("Невозможно создать корзину, которая не содержит товаров.".trim(), result.trim());
    }

    @Test
    public void EmptyBasketWithMissingGoods() {
        GoodsService goodsService = new GoodsService();
        String inputGoodsString = "Агава, 101.5\n Бумага 11.1";
        String inputBasketString = "Календарь 9";
        goodsService.loadGoodsIntoRepository(inputGoodsString, true);
        String result = goodsService.makeBasketFromString(inputBasketString);
        Assert.assertEquals("Невозможно создать корзину, которая не содержит товаров.\n\nНе удалось найти следующие товары:\nКалендарь\n".trim(), result.trim());
    }

    @Test
    public void Test() {
        GoodsService goodsService = new GoodsService();
        String inputGoodsString = "Агава, 101.5\nБумага, 11.3";
        String inputBasketString = "Агава 3\n";
        goodsService.loadGoodsIntoRepository(inputGoodsString, true);
        String result = goodsService.makeBasketFromString(inputBasketString);
        Assert.assertEquals(String.format((
                "Заказ №3 Фамилия Имя Отчество %s\n" +
                        "\n" +
                        "Название Цена  Количество Сумма\n" +
                        "Агава    101.5 3          304.5\n" +
                        "\n" +
                        "Итого: 304.5\n" +
                        "\n").trim(), new SimpleDateFormat("yyyy.MM.dd").format(new Date())), result.trim());
    }

    @Test
    public void ManyGoodsTest() {
        GoodsService goodsService = new GoodsService();
        String inputGoodsString = "Агава, 101.5\n Бумага, 11.3";
        String inputBasketString = "Агава 3\nБумага 4";
        goodsService.loadGoodsIntoRepository(inputGoodsString, true);
        String result = goodsService.makeBasketFromString(inputBasketString);
        Assert.assertEquals(String.format((
                "Заказ №1 Фамилия Имя Отчество %s\n" +
                        "\n" +
                        "Название Цена  Количество Сумма\n" +
                        "Агава    101.5 3          304.5\n" +
                        "Бумага   11.3  4          45.2\n" +
                        "\n" +
                        "Итого: 349.7\n").trim(), new SimpleDateFormat("yyyy.MM.dd").format(new Date())), result.trim());
    }

    @Test
    public void ManyGoodsWithMissingGoodsTest() {
        GoodsService goodsService = new GoodsService();
        String inputGoodsString = "Агава, 101.5\nБумага, 11.3";
        String inputBasketString = "Бумага 4\nАгава 3\nКалендарь 5";
        goodsService.loadGoodsIntoRepository(inputGoodsString, true);
        String result = goodsService.makeBasketFromString(inputBasketString);
        Assert.assertEquals(String.format((
                "Заказ №2 Фамилия Имя Отчество %s\n" +
                        "\n" +
                        "Название Цена  Количество Сумма\n" +
                        "Агава    101.5 3          304.5\n" +
                        "Бумага   11.3  4          45.2\n" +
                        "\n" +
                        "Итого: 349.7\n" +
                        "\n" +
                        "Не удалось найти следующие товары:\n" +
                        "Календарь\n").trim(), new SimpleDateFormat("yyyy.MM.dd").format(new Date())), result.trim());
    }
}
